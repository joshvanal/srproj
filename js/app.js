angular.module("vipApp",['ngRoute','ui.bootstrap'])
.config(function($routeProvider) {
  $routeProvider
   .when('/', {
    templateUrl: 'pages/home.html',
    controller: 'homeController',
  })
  .when('/weeklyReports', {
    templateUrl: 'pages/weekly.html',
    controller: 'weeklyController'
  })
   .when('/teamInfo', {
    templateUrl: 'pages/teamInfo.html',
    controller: 'infoController'
  })
  .otherwise({
    redirectTo: '/'
  });
 });